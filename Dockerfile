FROM python:3.8-alpine

RUN apk add --no-cache git
RUN pip3 install awsebcli --upgrade
